defmodule Payroll.Repo.Migrations.CreateEmployees do
  use Ecto.Migration

  def change do
    create table(:employees) do
      add :name, :string
      add :age, :integer
      add :sex, :string
      add :profession, :string
      add :salary, :decimal
      add :company_id, references(:companies, on_delete: :nothing)

      timestamps()
    end

    create index(:employees, [:company_id])
  end
end
