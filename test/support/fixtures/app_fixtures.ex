defmodule Payroll.AppFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Payroll.App` context.
  """

  @doc """
  Generate a company.
  """
  def company_fixture(attrs \\ %{}) do
    {:ok, company} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> Payroll.App.create_company()

    company
  end

  @doc """
  Generate a employee.
  """
  def employee_fixture(attrs \\ %{}) do
    {:ok, employee} =
      attrs
      |> Enum.into(%{
        age: 42,
        name: "some name",
        profession: "some profession",
        salary: "120.5",
        sex: "some sex"
      })
      |> Payroll.App.create_employee()

    employee
  end
end
