defmodule Payroll.AppTest do
  use Payroll.DataCase

  alias Payroll.App

  describe "companies" do
    alias Payroll.App.Company

    import Payroll.AppFixtures

    @invalid_attrs %{name: nil}

    test "list_companies/0 returns all companies" do
      company = company_fixture()
      assert App.list_companies() == [company]
    end

    test "get_company!/1 returns the company with given id" do
      company = company_fixture()
      assert App.get_company!(company.id) == company
    end

    test "create_company/1 with valid data creates a company" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %Company{} = company} = App.create_company(valid_attrs)
      assert company.name == "some name"
    end

    test "create_company/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = App.create_company(@invalid_attrs)
    end

    test "update_company/2 with valid data updates the company" do
      company = company_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %Company{} = company} = App.update_company(company, update_attrs)
      assert company.name == "some updated name"
    end

    test "update_company/2 with invalid data returns error changeset" do
      company = company_fixture()
      assert {:error, %Ecto.Changeset{}} = App.update_company(company, @invalid_attrs)
      assert company == App.get_company!(company.id)
    end

    test "delete_company/1 deletes the company" do
      company = company_fixture()
      assert {:ok, %Company{}} = App.delete_company(company)
      assert_raise Ecto.NoResultsError, fn -> App.get_company!(company.id) end
    end

    test "change_company/1 returns a company changeset" do
      company = company_fixture()
      assert %Ecto.Changeset{} = App.change_company(company)
    end
  end

  describe "employees" do
    alias Payroll.App.Employee

    import Payroll.AppFixtures

    @invalid_attrs %{age: nil, name: nil, profession: nil, salary: nil, sex: nil}

    test "list_employees/0 returns all employees" do
      employee = employee_fixture()
      assert App.list_employees() == [employee]
    end

    test "get_employee!/1 returns the employee with given id" do
      employee = employee_fixture()
      assert App.get_employee!(employee.id) == employee
    end

    test "create_employee/1 with valid data creates a employee" do
      valid_attrs = %{age: 42, name: "some name", profession: "some profession", salary: "120.5", sex: "some sex"}

      assert {:ok, %Employee{} = employee} = App.create_employee(valid_attrs)
      assert employee.age == 42
      assert employee.name == "some name"
      assert employee.profession == "some profession"
      assert employee.salary == Decimal.new("120.5")
      assert employee.sex == "some sex"
    end

    test "create_employee/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = App.create_employee(@invalid_attrs)
    end

    test "update_employee/2 with valid data updates the employee" do
      employee = employee_fixture()
      update_attrs = %{age: 43, name: "some updated name", profession: "some updated profession", salary: "456.7", sex: "some updated sex"}

      assert {:ok, %Employee{} = employee} = App.update_employee(employee, update_attrs)
      assert employee.age == 43
      assert employee.name == "some updated name"
      assert employee.profession == "some updated profession"
      assert employee.salary == Decimal.new("456.7")
      assert employee.sex == "some updated sex"
    end

    test "update_employee/2 with invalid data returns error changeset" do
      employee = employee_fixture()
      assert {:error, %Ecto.Changeset{}} = App.update_employee(employee, @invalid_attrs)
      assert employee == App.get_employee!(employee.id)
    end

    test "delete_employee/1 deletes the employee" do
      employee = employee_fixture()
      assert {:ok, %Employee{}} = App.delete_employee(employee)
      assert_raise Ecto.NoResultsError, fn -> App.get_employee!(employee.id) end
    end

    test "change_employee/1 returns a employee changeset" do
      employee = employee_fixture()
      assert %Ecto.Changeset{} = App.change_employee(employee)
    end
  end
end
