defmodule PayrollWeb.EmployeeController do
  use PayrollWeb, :controller

  alias Payroll.App
  alias Payroll.App.Employee

  def index(conn, params) do
    if Map.has_key?(params, "company") do
      %{"company" => company_id} = params
      employees = App.list_employees_by_company(company_id)
      render(conn, "index.html", employees: employees)
    else
      employees = App.list_employees()
      gender = App.count_employees_by_gender
      professions = App.count_employees_by_profession
      render(conn, "all.html", employees: employees, gender: gender, professions: professions)
    end
  end

  def new(conn, _params) do
    changeset = App.change_employee(%Employee{})
    companies = for company <- App.list_companies do [value: company.id, key: company.name] end
    render(conn, "new.html", changeset: changeset, companies: companies)
  end

  def create(conn, %{"employee" => employee_params}) do
    case App.create_employee(employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee created successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    employee = App.get_employee!(id)
    render(conn, "show.html", employee: employee)
  end

  def edit(conn, %{"id" => id}) do
    employee = App.get_employee!(id)
    changeset = App.change_employee(employee)
    companies = for company <- App.list_companies do [value: company.id, key: company.name] end
    render(conn, "edit.html", employee: employee, changeset: changeset, companies: companies)
  end

  def update(conn, %{"id" => id, "employee" => employee_params}) do
    employee = App.get_employee!(id)

    case App.update_employee(employee, employee_params) do
      {:ok, employee} ->
        conn
        |> put_flash(:info, "Employee updated successfully.")
        |> redirect(to: Routes.employee_path(conn, :show, employee))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", employee: employee, changeset: changeset)
    end
  end

  def delete(conn, %{"id" => id}) do
    employee = App.get_employee!(id)
    {:ok, _employee} = App.delete_employee(employee)

    conn
    |> put_flash(:info, "Employee deleted successfully.")
    |> redirect(to: Routes.employee_path(conn, :index))
  end
end
