defmodule PayrollWeb.PageController do
  use PayrollWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
