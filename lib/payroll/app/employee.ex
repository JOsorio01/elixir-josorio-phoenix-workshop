defmodule Payroll.App.Employee do
  use Ecto.Schema
  import Ecto.Changeset

  schema "employees" do
    field :age, :integer
    field :name, :string
    field :profession, :string
    field :salary, :decimal
    field :sex, :string
    belongs_to :company, Payroll.App.Company

    timestamps()
  end

  @doc false
  def changeset(employee, attrs) do
    employee
    |> cast(attrs, [:name, :age, :sex, :profession, :salary, :company_id])
    |> validate_required([:name, :age, :sex, :profession, :salary, :company_id])
  end

end
